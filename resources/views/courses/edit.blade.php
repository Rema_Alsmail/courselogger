@extends('layouts.app')

@section('title', __('Edit course'))

@section('content')

    <h2>{{__('Edit course')}} : {{$course->title}}</h2>
    <form action="{{route('courses.update', $course)}}" method="post">
        @csrf

        @method('Patch')
        <div class="form-group">
            <label class="col-form-label" for="title">Course title: </label>
            <input type="text"
                   class="form-control"
                   name="title" placeholder="Ex:HTML"
                   id="title"
                   @isset($course) value="{{$course->title}}" @endisset>
        </div>

        <div class="form-group">
            <label class="col-form-label" for="content">Course description: </label>
            <textarea class="form-control"
                      name="content" id="content"
                      cols="30"
                      rows="10"
                      placeholder="Create a HTML web page...">
        @isset($course){{$course->content}}@endisset
    </textarea>
        </div>

        <div class="form-group ">
            <button type="submit" class="btn btn-success">Submit</button>
        </div>

    </form>

@endsection
