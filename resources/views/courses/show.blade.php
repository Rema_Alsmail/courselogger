@extends('layouts.app')

@section('title', $course->title)

@section('content')


    {{--Course Info--}}
    <div class="card">

        <h4 class="card-header">
            {!! nl2br($course->title)!!}
        </h4>

        <div class="card-body">
            {!! nl2br($course->content) !!}
        </div>


        {{--Author Info--}}
        <div class="card-footer">
            <div><b>{{__('Author')}} : </b> {{$course->user->name}}</div>
            <div><b>{{__('Created at')}} :</b> {{$course->created_at}}</div>
            <div><b>{{__('Updated at')}} :</b> {{$course->updated_at}}</div>
        </div>

    </div>


    {{--Comments Section--}}
    <div class="mt-5">
        @include('courses.partials._replys', ['comments' => $course->comments, 'course_id' => $course->id])
    </div>



    {{--Writing Comment Section--}}
    <div class="card  mt-5">
        @auth
            <h5 class="card-header bg-secondary text-gray">{{__('Type your comment here')}}</h5>

            <div class="card-body">
                <form method="post" action="{{route('comments.store', $course->id)}}">
                    @csrf
                    <div class="form-group">

                        <label for="content">
                            {{__('Content')}}
                        </label>

                        <textarea class="form-control"
                                  placeholder="{{__('Type your comment here')}}"
                                  name="content"
                                  cols="30" rows="10">
                        </textarea>

                    </div>

                    <div class="form-group">
                        <input class="btn btn-outline-success  py-0" value="Save" type="submit"/>
                    </div>
                </form>
            </div>
        @else
            <a class="m-1" href="{{route('login')}}">{{__('Log in to comment')}}</a>
        @endauth
    </div>
@endsection
