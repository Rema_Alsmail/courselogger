@extends('layouts.app')

@section('title', __('Home page'))
<style>
    #addCourse{
        position: absolute;
        top: 80px;
        right: 190px;
    }
</style>
@section('content')

    <div class="container text-right ">
        <h3 class="text-left text-primary active mb-3">{{__('The Courses')}}</h3>
        <hr class="my-4">
        <a href="{{route('courses.create')}}" id="addCourse" class=" btn btn-lg btn-outline-primary mb-3 ">{{__('Add New Course')}}</a>
    </div>

    <div class="row">
        @forelse($courses as $course)
            @include('courses.partials._course_card')
        @empty
            {{__('No courses yet')}}
        @endforelse
    </div>
@endsection
