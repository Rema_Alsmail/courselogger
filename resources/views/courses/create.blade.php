@extends('layouts.app')

@section('title', __('Create course'))

@section('content')

    <h2>{{__('Create course')}}</h2>
    <form action="{{route('courses.store')}}" method="post">
        @csrf
        <div class="form-group">
            <label class="col-form-label" for="title">Course title: </label>
            <input type="text" class="form-control" name="title" placeholder="Ex:HTML" id="title">
        </div>

        <div class="form-group">
            <label class="col-form-label" for="content">Course description: </label>
            <textarea class="form-control"
                      name="content" id="content"
                      cols="30"
                      rows="10"
                      placeholder="Create a HTML web page...">
        @isset($course){{$course->content}}@endisset
    </textarea>
        </div>

        <div class="form-group ">
            <button type="submit" class="btn btn-success">Submit</button>
        </div>

    </form>

@endsection
