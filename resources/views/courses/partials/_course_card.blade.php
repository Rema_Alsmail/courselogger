<div class="col-md-4 pb-3">
    <div class="card  h-100">
        <h4 class="card-header">
            <a href="{{route('courses.show', $course->id)}}">{{$course->title}}
            </a>
        </h4>
        <div class="card-body">
            {!! nl2br($course->content) !!}
        </div>

        @if(Auth::user()!=null && Auth::user()->id==$course->user_id)
        <div class="card-footer">
            <a href="{{route('courses.edit', $course)}}" class="btn btn-outline-warning ">
                {{__('Edit')}}
            </a>
            <form method="post" action="{{route('courses.destroy', $course)}}" style="display: inline-block">
                @method('Delete')
                @csrf
                <button class="btn btn-outline-danger"
                        onclick="return confirm('{{__('Are you sure?')}}')"> {{__('Delete')}}</button>
            </form>

        </div>
            @endif
    </div>
</div>
