
<div class="container card">
    <h3 class="mt-2 ">{{__('Comments')}}</h3>
    <hr>

    <div class="card-body">
        @forelse($course->comments as $comment)
            <div class="card">
                <div class="card-header">
                    {{ '@'.$comment->user->name }} :-
                </div>

                <div class="card-body ml-4">
                    {{$comment->content}}
                </div>

            @if(Auth::user()!=null)
                <div class="card-footer">

{{--Reply on Comments Section--}}
                    <form method="post" action="{{route('reply.store',$comment->id)}}">
                        @csrf
                        <div class=" form-group">
                            <input type="text" name="content" class="form-control" placeholder='Type your reply here.'/>
                        </div>

                        <div class="form-group">
                            <input type="submit" class="btn btn-sm btn-outline-primary py-0" style="font-size: 0.8em;" value="Reply"/>
                        </div>
                    </form>



                    @if( Auth::user()->id==$comment->user_id)
{{--Delete a comment on course Section--}}
                    <form method="post" action="{{route('reply.destroy', $comment)}}">
                        @method('Delete')
                        @csrf
                        <button style="position: absolute; bottom: 28px; left: 80px; font-size: 0.8em;"
                                onclick="return confirm('{{__('Are you sure?')}}')"> {{__('Delete')}} <i class="fa fa-trash-o"></i></button>
                    </form>
                    @endif
                </div>
            @endif
            </div>

            <br>

            <div class="ml-5 card">
                @foreach($comment->replies as $reply)
                    <div class="card m-1 ">
                        <div class="card-header">
                            <span class="">{{ '@'.$reply->user->name }}, {{__('reply:- ')}}</span>
                        </div>

                        <div class="card-body">
                            <div class="ml-5">
                                {{$reply->content}}
                            </div>
                        </div>

                    @if(Auth::user()!=null)
                        <div class="card-footer">
                            <form method="post" action="{{route('reply.store', $comment->id )}}">
                                @csrf
                                <div class=" form-group">
                                    <input type="text" name="content" class="form-control"
                                           placeholder='Type your reply here.'/>
                                </div>
                                <div class="form-group">
                                    <input type="submit" class="btn btn-sm btn-outline-primary py-0"
                                           style="font-size: 0.8em;" value="Reply"/>

                                </div>
                            </form>


                            @if(Auth::user()->id==$reply->user_id)
{{--Delete a reply on comment Section--}}
                            <form method="post" action="{{route('reply.destroy', $reply)}}">
                                @method('Delete')
                                @csrf
                                <button id="delRep" style="position: absolute; bottom: 28px; left: 80px; font-size: 0.8em;" class="btn btn-sm btn-outline-danger py-0"
                                        onclick="return confirm('{{__('Are you sure?')}}')"> {{__('Delete')}}</button>
                            </form>
                            @endif
                        </div>
                        @endif
                    </div>

                @endforeach
            </div>
            <br>
        @empty
            <p style="color: gray;">{{__('No comments yet')}}</p>
        @endforelse

    </div>

</div>
