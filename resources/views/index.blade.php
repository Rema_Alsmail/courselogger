@extends('layouts.app')
<style>

    .jumbotron {
        background-image: url("{{URL::asset('img/5514.jpg')}}");
        background-size: cover;
        background-repeat: no-repeat;
        background-position: center;
        position: relative;
        height: 670px;
    }

    #desc {
        position: absolute;
        border-radius: 40px;
        bottom: 40px;
        width: 95%;
        padding-top: 5px;
        margin-top: 35px;
        text-align: center;
        font-size: 30px;
        background-color: #eaf3fae0;
    }

    #wel {
        text-align: center;
        font-size: 50px;
    }

    #IndexAddCourse{
        position: absolute;
        top: 806px;
        right: 190px;
    }
</style>

@section('title', __('Home'))

@section('content')

    <div class="jumbotron">
        <div class="text-primary">
            <h1 id="wel">{{__('Welcome')}} </h1>
            <h5 id="desc" class="text-primary">{{config('app.name')}}, is a website for logging your courses and share
                them with others.</h5>
        </div>
    </div>


    <hr class="my-4">

    <div class="container text-right">
        <h3 class="text-left text-primary active mb-3">{{__('The Latest Courses')}}</h3>
        <a href="{{route('courses.create')}}" id="IndexAddCourse" class=" btn btn-lg btn-outline-primary mb-3 ">{{__('Add New Course')}}</a>
        <hr class="my-4">
    </div>

    <div class="row">

        @forelse($courses as $course)

            @include('courses.partials._course_card')

        @empty

            {{__('You do not have any courses yet')}}

        @endforelse
    </div>

    <a href="{{route('courses.index')}}" class="btn btn-outline-primary btn-sm">{{__('Browse all courses')}}</a>

    <hr>
@endsection
