<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

//Route::get('/home', 'HomeController@index')->name('home');



Auth::routes();


Route::get('/', 'PageController@index')->name('home');

Route::resource('courses', 'CourseController');

Route::resource('comments', 'CommentController');

Route::post('comments/store/{course}', 'CommentController@store')->name('comments.store');

Route::post('/reply/store/{comment}', 'CommentController@replyStore')->name('reply.store');
//
Route::delete('/reply/destroy/{reply}', 'CommentController@replyDestroy')->name('reply.destroy');


//Route::post('reply/{course}', 'CommentController@store')->name('comments.store');

//
//
//
//Route::post('/comment/store/{course}', 'CommentController@store')->name('comments.store');
//Route::post('/comment/destroy/{comment}', 'CommentController@store')->name('comments.destroy');
//
//Route::post('/reply/store/{comment}', 'CommentController@replyStore')->name('reply.store');
//
////Route::post('reply/destroy/{reply}', 'CommentController@replyDestroy')->name('reply.destroy');
//
//
////Route::post('reply/{course}', 'CommentController@store')->name('comments.store');
