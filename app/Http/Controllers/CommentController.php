<?php

namespace App\Http\Controllers;

use App\Comment;
use App\Course;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;

class CommentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function index()
    {
        $this->middleware('auth');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('comment.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Course $course)
    {

        $request['user_id'] = Auth::id();

        $request->validate([
            'content' => 'min:1 | required',
        ]);


//        Comment::create($request->all());
        $request['commentable_id']= $course;

        $course->comments()->create($request->all());

        return redirect()->back();


    }


    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function replyStore(Request $request, Comment $comment)
    {
        $request['user_id'] = Auth::id();
        $request->validate([
            'content' => 'min:1 | required',
        ]);

        $comment->replies()->create($request->all());

        return redirect()->back();

    }

    /**
     * Display the specified resource.
     *
     * @param \App\Comment $comment
     * @return \Illuminate\Http\Response
     */
    public function show(Comment $comment)
    {

        return View('show', compact('comment'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Comment $comment
     * @return \Illuminate\Http\Response
     */
    public function edit(Comment $comment)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Comment $comment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Comment $comment)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Comment $comment
     * @return \Illuminate\Http\Response
     */
    public function destroy(Comment $comment)
    {
        if (Auth::id() != $comment->user_id) {
            return abort(401);
        }

        $comment->delete();
        return redirect()->back();
    }

    public function replyDestroy(Comment $reply)
    {
        if (Auth::id() != $reply->user_id) {
            return abort(401);
        }

        $reply->delete();
        return redirect()->back();
    }
}
