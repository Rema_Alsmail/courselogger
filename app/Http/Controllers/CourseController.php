<?php

namespace App\Http\Controllers;

use App\Course;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use Illuminate\Support\Facades\Auth;


class CourseController extends Controller
{
    public function __construct()
    {
        //User Auth
        $this->middleware('auth')->except('index','show');;
    }

    public function index()
    {
        $courses = Course::orderBy('id', 'Desc')->get();
        return view('courses.index', compact('courses'));
    }

    public function create()
    {
        return view('courses.create');
    }

    public function store(Request $request)
    {

        $user = Auth::user();

        $request->validate([
            'title' => 'max:50 | min:10 | required',
            'content' => 'min:20 | required',

        ]);

        $courses = $user->courses()->create($request->all());

        return redirect()->to('/');
    }

    public function show($course)
    {
//        $reply = DB::table('comments')->where('course_id', '==',19 )->first();
//        dd($reply);
        $course = Course::with('comments', 'comments.replies', 'comments.user', 'comments.replies.user')->findOrFail($course);
        //dd($course->comments());
        return view('courses.show', compact('course'));
    }

    public function edit(Course $course)
    {
        if (Auth::id() != $course->user_id) {
            return abort(401);
        }

        return view('courses.edit', compact('course'));
    }

    public function update(Request $request, Course $course)
    {
        if (Auth::id() != $course->user_id) {
            return abort(401);
        }

        $request->validate([
            'title' => 'max:50 | min:10 | required',
            'content' => 'min:20 | required',
        ]);

        $course->update($request->all());

        return redirect()->to('/');

    }

    public function destroy(Course $course)
    {
        if (Auth::id() != $course->user_id) {
            return abort(401);
        }

        $course->delete();
        return redirect()->back();
    }
}
