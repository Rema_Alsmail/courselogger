<?php

namespace App\Http\Controllers;

use App\Course;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PageController extends Controller
{


    public function __construct()
    {
//        $this->middleware(['auth'])->except('index');
    }


    public function index()
    {
        $courses = Course::take(3)->orderBy('id', 'Desc')->get();

        return view('index', compact('courses'));
    }
}
