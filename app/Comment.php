<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Relation;

class Comment extends Model
{

    protected $fillable = [
        'course_id',
        'user_id',
        'content',
        'reply_id',
        'commentable_id',
        'commentable_type',
        'parent_id'

    ];

    public function commentable(): Relation
    {
        return $this->morphTo();
    }

    public function user()
    {
        //O2M
        return $this->belongsTo(User::class);
    }

//    public function course()
//    {
//        //O2M
//        return $this->belongs(Course::class, 'commentable');
//    }

//    public function replies()
//    {
//        return $this->hasMany(Comment::class,'parent_id');
//    }

    public function replies(): Relation
    {
        return $this->morphMany(self::class, 'commentable');
    }
}
